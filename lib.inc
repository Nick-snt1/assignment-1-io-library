%define     NEWLINE     0xA
%define     SPACE       0x20
%define     TAB         0x9

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 ; rdi contains exit code
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax    ; save length
    mov rax, 1      ; syscall id
    mov rdi, 1      ; out descriptor
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    test rax, rax
    jns .end
    push rax
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.end:

print_uint:
    mov rax, rdi
    mov rdi, 0xA    ; divider
    push 0
.loop:
    xor rdx, rdx
    div rdi
    add rdx, 0x30 
    push rdx
    test rax, rax
    jnz .loop
.next_loop:
    pop rdi
    cmp rdi, 0
    je .end
    call print_char
    jmp .next_loop
.end:
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
    mov rax, 1
.loop:
    mov dl, [rdi+r8]
    mov cl, [rsi+r8]
    cmp dl, cl
    je .next
    xor rax, rax
    jmp .end
.next:
    cmp dl, 0
    je .end
    inc r8
    jmp .loop
.end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax      ; syscall n
    xor rdi, rdi      ; file descriptor
    mov rdx, 1        ; n bytes
    push 0            
    mov rsi, rsp      ; buff addr
    syscall
    pop rax           
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rdx, rdx       
.check_space:
    xor  rax, rax
    test rdx, rdx
    jne .next
.loop:
    push rdi
    push rsi
    push rdx
    call read_char
    pop  rdx
    pop  rsi
    pop  rdi
    
    cmp rax, NEWLINE
    je .check_space
    cmp rax, SPACE
    je .check_space
    cmp rax, TAB
    je .check_space
.next:   
    cmp rdx, rsi
    je .error
    mov [rdi+rdx], rax
    cmp rax, 0
    je  .end
    inc rdx 
    jmp .loop
.error:
    xor rdi, rdi
.end: 
    mov rax, rdi  
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative
    jmp .positive
.negative:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret
.positive: 
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax    
    xor rcx, rcx   
    xor r8, r8 
    mov r10, 0xA    ; multiplier
.loop:
    cmp byte[rdi+r8], 0x30
    jb .end
    cmp byte[rdi+r8], 0x39
    ja .end
    
    mov cl, [rdi+r8]
    sub cl, 0x30

    mul r10
    add rax, rcx

    inc r8
    jmp .loop
.end:
    mov rdx, r8
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    
    push rsi
    push rdi
    push rdx
    call string_length
    pop rdx
    pop rdi
    pop rsi

    cmp rax, rdx
    jae .error

    xor rax, rax
    xor rcx, rcx
.loop:
    mov cl, [rdi+rax]
    mov [rsi+rax], cl
    cmp cl, 0
    je .end
    inc rax
    jmp .loop
.error:
    xor rax, rax
.end:
    ret



